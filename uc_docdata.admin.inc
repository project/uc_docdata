<?php
//$Id

/**
 * @file
 * Integration of DocData payment method into Ubercart
 */

/**
 * Administrative section for the uc_docdata module
 */
function uc_docdata_admin_screen() {
	drupal_add_js(drupal_get_path('module', 'uc_docdata') . '/uc_docdata.js');
	
	$orders = _uc_docdata_orders();
	
	//Table headers
	$headers = array(t('Order ID'), t('Customer'), t('Total'), t('Status'), t('DocData status'), t('Set payment ok'));
	
	//Table rows
	$rows = array();
	
	foreach ($orders as $order) {
		$row = array();
		
		$docdatastatus = _uc_docdata_get_docdata_status($order->order_id);
		
		$row[] = $order->order_id;
		$row[] = theme('uc_docdata_customer', $order);
		$row[] = uc_currency_format($order->order_total);
		$row[] = $order->order_status;
		$row[] = $docdatastatus;
		$row[] = ($docdatastatus == 'paid') ? '<input type="button" value="Click!" onclick="javascript:uc_docdata_payment_ok(this, ' . $order->order_id . ')" />' : '';
		
		$rows[] = $row;
	}
	
	return theme('table', $headers, $rows);
}