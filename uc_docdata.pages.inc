<?php
//$Id

/**
 * @file
 * Integration of DocData payment method into Ubercart
 */

/**
 * Function will be fired after return from docdata
 */
function uc_docdata_return() {
	uc_docdata_handle_return();
}