function uc_docdata_payment_ok(button, order_id) {
	params = 'order_id=' + order_id;
	
	$.ajax({
		url: '/docdata/pay_order',
		data: params,
		success: function (data) {
			if (data == 1) {
				$(button).hide();
			}
		}
	});
}
