<?php
//$Id

/**
 * @file
 * Integration of DocData payment method into Ubercart
 */

/**
 * Theme function for the customer display in the admin section
 */
function theme_uc_docdata_customer($order) {
	$output = '';
	
	$output .= $order->billing_first_name . ' ' . $order->billing_last_name;
	$output .= '<br />';
	$output .= $order->billing_street1;
	$output .= '<br />';
	$output .= $order->billing_postal_code . ' ' . $order->billing_city;
	$output .= '<br />';
	$output .= $order->billing_country;
	
	return $output;
}