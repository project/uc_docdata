<?php
//$Id

/**
 * @file
 * Integration of DocData payment method into Ubercart
 */

/**
 * Set the status of an order to payment_received with an Ajax call
 */
function uc_docdata_pay_order() {
	if (user_access('administer docdata payments')) {
		$order_id = $_GET['order_id'];
		uc_order_update_status($order_id, 'payment_received');
		echo '1';
	} else {
		echo '0';
	}
}